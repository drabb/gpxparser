﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPXParser
{

    [DebuggerDisplay("Lat = {Latitude}, Lon = {Longitude}")]
    public class Point
    {
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }

        public Point(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }

    public class Haversine
    {
        

        public static double ToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        public static double HaversineDistance(Point a, Point b)
        {
            const double radius = 3956.6; // *5280; // in feet
            double latitude = ToRadian(b.Latitude - a.Latitude) / 2;
            double longitude = ToRadian(b.Longitude - a.Longitude) / 2;
            
            double x = Math.Sin(latitude) * Math.Sin(latitude) + Math.Sin(longitude) * Math.Sin(longitude) *
                                                                 Math.Cos(ToRadian(a.Latitude)) * Math.Cos(ToRadian(b.Latitude));
            double y = 2 * Math.Atan2(Math.Sqrt(x), Math.Sqrt(1 - x));

            return radius * y;
        }

    }
}
