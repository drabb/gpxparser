﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GPXParser
{
    public class TrackPoint
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public decimal Elevation { get; set; }
        public DateTime TimeStamp { get; set; }
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            string json_row_format = "{{ \"c\": [ {{\"v\": {0}}}, {{ \"v\": {1}}}, {{ \"v\": \"{2}\"}}] }},";
            string json_row = string.Empty;
            string rows = string.Empty;

            List<TrackPoint> trackPoints = GetTrackPoints(args[0]);

            double totalDistance = 0;
            
            TrackPoint start = trackPoints.First();

            int totalTracks = trackPoints.Count;
            int totalRows = 0;

            json_row = string.Format(json_row_format, 0, start.Elevation.ToString(), start.Elevation.ToString() + " ft.");
            rows = json_row;

            for (int k = 0; k < trackPoints.Count - 1; k++)
            {
                var currentPoint = trackPoints[k + 1];
                var currentElev = trackPoints[k].Elevation;
                var nextElev = currentPoint.Elevation;

                totalDistance += CalculateDistance(trackPoints[k], currentPoint);

                if (Math.Round(currentElev) != Math.Round(nextElev))
                {
                    totalRows++;

                    //if (totalRows % 5 == 1)
                    //{
                        json_row = string.Format(
                            json_row_format, 
                            Math.Round(totalDistance, 2), Math.Round(currentPoint.Elevation, 2),
                            Math.Round(currentPoint.Elevation, 2).ToString() + " ft.");
                        rows += (json_row + Environment.NewLine);
                    //}
                    
                }
            }

            rows = rows.Substring(0, rows.Length - 3);  // remove trailing newline & comma

            StringBuilder jsonData = new StringBuilder();

            jsonData.AppendLine("{ \"cols\": [ { \"label\": \"Distance\", \"type\": \"number\" }, { \"label\": \"Elevation\", \"type\": \"number\" }, { \"role\": \"tooltip\", \"type\": \"string\", \"p\" : { \"role\" : \"tooltip\" } }");
            jsonData.AppendLine("], \"rows\": [");
            jsonData.Append(rows);
            jsonData.AppendLine("]}");
            
            string path = "data.json";

            // This text is added only once to the file. 
            if (!File.Exists(path))
            {
                // Create a file to write to. 
                File.WriteAllText(path, jsonData.ToString());
            }

        }

        static List<TrackPoint> GetTrackPoints(string filename)
        {

            XDocument xdoc = XDocument.Load(filename);
            XNamespace gpx = XNamespace.Get("http://www.topografix.com/GPX/1/1");

            return
                (
                    from trkpt in xdoc.Descendants(gpx + "trkpt")
                    select new TrackPoint()
                    {
                        Longitude = double.Parse(trkpt.Attribute("lon").Value),
                        Latitude = double.Parse(trkpt.Attribute("lat").Value),
                        Elevation = decimal.Parse(trkpt.Element(gpx + "ele").Value),
                        TimeStamp = DateTime.Parse(trkpt.Element(gpx + "time").Value)
                    }
                )
                .ToList();
            
        }

        static double CalculateDistance(TrackPoint start, TrackPoint end)
        {
            GPXParser.Point startPoint = new Point(start.Latitude, start.Longitude);
            GPXParser.Point endPoint = new Point(end.Latitude, end.Longitude);

            return Haversine.HaversineDistance(endPoint, startPoint);
        }
    }
}
